# Nicolas Delcourt
## _Hey ! 👋  Le code de mon site personnel/portfolio_

L'objectif de ce projet était de refaire avec des outils moderne mon site. (il était simplement fait en HTML, CSS, JS)

Nouvelle à l'aide de framworks et outils moderne.

- VueJS 
- Vuetify
- Beaucoup de café  

## Installation en local

Le site à besoin d'une version [Node.js](https://nodejs.org/) v10 ou supérieur et de la dernière version de [Yarn](https://yarnpkg.com/getting-started/install).

Pour lancer l'application en local.

```sh
cd nicolas.fr
yarn 
yarn run serve
```

## Demo en Ligne 

Vous pouvez visiter la demo du site 
en cous de création 

<a href="https://app.daily.dev/Taggada"><img src="https://api.daily.dev/devcards/65950a1cf97d4efdb8285114eb221cf6.png?r=6k8" width="400" alt="Nicolas's Dev Card"/></a>
